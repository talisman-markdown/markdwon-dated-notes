const vscode = require("vscode");
const dateFormat = require("dateformat");
const createCompletionItem = ({
	snippet,
	date,
	detail
}) => {
	const completionItem = new vscode.CompletionItem(snippet, vscode.CompletionItemKind.Snippet);
	completionItem.insertText = getDailyNoteLink(date);
	completionItem.detail = `${completionItem.insertText} - ${detail}`;

	return completionItem;
};

const computedSnippets = [
	(days) => {
			const today = new Date();
			return {
					detail: `Insert a date ${days} day(s) from now`,
					snippet: `/+${days}d`,
					date: new Date(today.getFullYear(), today.getMonth(), today.getDate() + days),
			};
	},
	(weeks) => {
			const today = new Date();
			return {
					detail: `Insert a date ${weeks} week(s) from now`,
					snippet: `/+${weeks}w`,
					date: new Date(today.getFullYear(), today.getMonth(), today.getDate() + 7 * weeks),
			};
	},
	(months) => {
			const today = new Date();
			return {
					detail: `Insert a date ${months} month(s) from now`,
					snippet: `/+${months}m`,
					date: new Date(today.getFullYear(), today.getMonth() + months, today.getDate()),
			};
	},
	(years) => {
			const today = new Date();
			return {
					detail: `Insert a date ${years} year(s) from now`,
					snippet: `/+${years}y`,
					date: new Date(today.getFullYear() + years, today.getMonth(), today.getDate()),
			};
	},
];
const computedCompletions = {
	provideCompletionItems: (document, position, _token, _context) => {
			if (_context.triggerKind === vscode.CompletionTriggerKind.Invoke) {
					// if completion was triggered without trigger character then we return [] to fallback
					// to vscode word-based suggestions (see https://github.com/foambubble/foam/pull/417)
					return [];
			}
			const range = document.getWordRangeAtPosition(position, /\S+/);
			const snippetString = document.getText(range);
			const matches = snippetString.match(/(\d+)/);
			const number = matches ? matches[0] : '1';
			const completionItems = computedSnippets.map(item => {
					const completionItem = createCompletionItem(item(parseInt(number)));
					completionItem.range = range;
					return completionItem;
			});
			// We still want the list to be treated as "incomplete", because the user may add another number
			return new vscode.CompletionList(completionItems, true);
	},
};

const daysOfWeek = [{
		day: 'sunday',
		index: 0
	},
	{
		day: 'monday',
		index: 1
	},
	{
		day: 'tuesday',
		index: 2
	},
	{
		day: 'wednesday',
		index: 3
	},
	{
		day: 'thursday',
		index: 4
	},
	{
		day: 'friday',
		index: 5
	},
	{
		day: 'saturday',
		index: 6
	},
];
const generateDayOfWeekSnippets = () => {
	const getTarget = (day) => {
		const target = new Date();
		const currentDay = target.getDay();
		const distance = (day + 7 - currentDay) % 7;
		target.setDate(target.getDate() + distance);
		return target;
	};
	const snippets = daysOfWeek.map(({
		day,
		index
	}) => {
		const target = getTarget(index);
		return {
			date: target,
			detail: `Get a daily note link for ${day}`,
			snippet: `/${day}`,
		};
	});
	return snippets;
};

const getDailyNoteLink = (date) => {
	var name = dateFormat(date, "isoDate");
	return `[[${name}]]`;
};

const completions = {
	provideCompletionItems: (document, position, _token, _context) => {
		if (_context.triggerKind === vscode.CompletionTriggerKind.Invoke) {
			// if completion was triggered without trigger character then we return [] to fallback
			// to vscode word-based suggestions (see https://github.com/foambubble/foam/pull/417)
			return [];
		}
		const range = document.getWordRangeAtPosition(position, /\S+/);
		const completionItems = [
			...snippets.map(item => {
				const completionItem = createCompletionItem(item());
				completionItem.range = range;
				return completionItem;
			}),
			...generateDayOfWeekSnippets().map(item => {
				const completionItem = createCompletionItem(item);
				completionItem.range = range;
				return completionItem;
			}),
		];
		return completionItems;
	},
};

const snippets = [
	() => ({
		detail: "Insert a link to today's daily note",
		snippet: '/day',
		date: new Date(),
	}),
	() => ({
		detail: "Insert a link to today's daily note",
		snippet: '/today',
		date: new Date(),
	}),
	() => {
		const today = new Date();
		return {
			detail: "Insert a link to tomorrow's daily note",
			snippet: '/tomorrow',
			date: new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1),
		};
	},
	() => {
		const today = new Date();
		return {
			detail: "Insert a link to yesterday's daily note",
			snippet: '/yesterday',
			date: new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1),
		};
	},
];

function activate(context) {
	let o = vscode.commands.registerCommand("markdown-dated-notes.helloWorld", (function () {
		vscode.window.showInformationMessage("Hello World from Markdown Dated Notes!")
	}));
	let complsreg = vscode.languages.registerCompletionItemProvider('markdown', completions, '/');
	let computedComplsreg = vscode.languages.registerCompletionItemProvider('markdown', computedCompletions, '/', '+');
	context.subscriptions.push(o, complsreg, computedComplsreg)
}

function deactivate() {}
module.exports = {
	activate: activate,
	deactivate: deactivate
};